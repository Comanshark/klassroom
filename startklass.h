#ifndef STARTKLASS_H
#define STARTKLASS_H

#include <QWidget>

namespace Ui {
class StartKlass;
}

class StartKlass : public QWidget
{
    Q_OBJECT

public:
    explicit StartKlass(QWidget *parent = nullptr);
    ~StartKlass();

signals:
    void newButtonClicked();
    void openButtonClicked();

private:
    Ui::StartKlass *ui;
};

#endif // STARTKLASS_H
