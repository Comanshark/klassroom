#include "newklass.h"
#include "ui_newklass.h"

#include "comboboxdelegate.h"
#include "spindelegate.h"
#include "klassproyect.h"

#include <QStandardItemModel>
#include <QInputDialog>
#include <QIcon>
#include <QFile>
#include <QDir>
#include <QFileDialog>
#include <QMessageBox>
#include <QTextStream>
#include <QVector>

NewKlass::NewKlass(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::NewKlass),
    mKlassModel(new QStandardItemModel(this)),
    mStudentModel(new QStandardItemModel(this)),
    mPlanningModel(new QStandardItemModel(this))
{        
    ui->setupUi(this);

    QRegExp regExp("[0-2][0-9][0][0-9]");
    ui->txtSection->setValidator(new QRegExpValidator(regExp));

    // FILL PERODY COMBOBOX.
    QStringList Perody = {"I", "II", "III"};
    ui->cbPerody->addItems(Perody);

    // START KLASS AND STUDENT TABLE, AND PLANNING TREE.
    startTables();
    loadKlassTable();

    // START BUTTONS FOR PLANING
    ui->BtnAdd->addActions({ui->actionAddPartial, ui->actionAddWork});

    // CONECTIONS
    connect(mKlassModel, SIGNAL(itemChanged(QStandardItem *)), this, SLOT(klassDataChanged(QStandardItem *))); // changed class data.
    connect(mStudentModel, SIGNAL(itemChanged(QStandardItem *)), this, SLOT(studentDataChanged(QStandardItem *))); // changed student data.
    connect(mPlanningModel, SIGNAL(itemChanged(QStandardItem *)), this, SLOT(planningDataChanged(QStandardItem *))); // changed planning data.
}

NewKlass::~NewKlass()
{
    delete ui;
}

bool NewKlass::createProyect(KlassProyect *const proyect)
{
    const int StudentCount = mStudentModel->rowCount() - 1;
    const int PartialCount = mPlanningModel->rowCount();

    /// IF A NEW KLASS CONTAIN NOT VALID DATA
    if (ui->tvKlass->selectionModel()->selectedIndexes().at(0).data().toString().isEmpty() ||
            ui->tvKlass->selectionModel()->selectedIndexes().at(1).data().toString().isEmpty()) {
        QMessageBox::information(this, tr("Faltan argumentos"), tr("No ha seleccionado un nombrecódigo de clase."));
        return false;
    }

    if (ui->txtSection->text().isEmpty()) {
        QMessageBox::information(this, tr("Faltan argumentos"), tr("No ha escrito una hora de sección."));
        return false;
    }

    if (StudentCount == 0) {
        QMessageBox::information(this, tr("No hay alumnos"), tr("No ha insertado ningún alumno"));
        return false;
    }

    if (PartialCount == 0) {
        QMessageBox::information(this, tr("No hay planificación"), tr("No ha planificato nada aún"));
        return false;
    }

    /// SETTING NAME PROYECT.
    QString Code = ui->tvKlass->selectionModel()->selectedIndexes().at(0).data().toString();
    QString Name = ui->tvKlass->selectionModel()->selectedIndexes().at(1).data().toString() +
            " " + ui->cbPerody->currentText() + "PAC-" + ui->spinYear->text();
    QString Section = ui->txtSection->text();

    proyect->setName(Name, Code, Section);

    /// SETTING STUDENTS IN THE PROYECT
    for (int Row = 0; Row < StudentCount; ++Row) {
        bool stateStudent = mStudentModel->item(Row, 0) == nullptr || mStudentModel->item(Row,1) == nullptr ||
                            mStudentModel->item(Row, 0)->text().isEmpty() || mStudentModel->item(Row, 1)->text().isEmpty();

        if (!stateStudent) {
            proyect->setStudent(mStudentModel->item(Row, 1)->text(), mStudentModel->item(Row, 0)->text());
        }
    }

    /// SETTING PARTIALS IN THE PROYECT
    for (int Row = 0; Row < PartialCount; ++Row) {
        QString namePartial = mPlanningModel->item(Row, 0)->text();
        double pondPartial = mPlanningModel->item(Row, 2)->text().toDouble();

        proyect->setPartial(namePartial, "", pondPartial);
    }


    /// SETTING WORK/TEST IN THE PROYECT
    for (int Parent = 0; Parent < PartialCount; ++Parent) {
        for (int childRow = 0; childRow < mPlanningModel->item(Parent)->rowCount(); ++childRow) {
            QString name = mPlanningModel->item(Parent)->child(childRow, 0)->text();
            bool type = (mPlanningModel->item(Parent)->child(childRow, 1)->text() == tr("Acumulativo") ? true : false);
            double pond = mPlanningModel->item(Parent)->child(childRow, 2)->text().toDouble();

            if (type) {
                proyect->setWork(static_cast<uint>(Parent), name, "", pond);
            }
            else {
                proyect->setTest(static_cast<uint>(Parent), name, "", pond);
            }
        }
    }

    saveKlass();

    return true;
}

void NewKlass::startTables()
{
    /// START KLASS TABLE
    mKlassModel->setHorizontalHeaderLabels({tr("Código"), tr("Nombre")});
    mKlassModel->setColumnCount(2);
    mKlassModel->setRowCount(1);
    ui->tvKlass->setModel(mKlassModel);
    ui->tvKlass->selectRow(0);

    /// START STUDENT TABLE
    mStudentModel->setHorizontalHeaderLabels({tr("No. de cuenta"), tr("Nombre")});
    mStudentModel->setColumnCount(2);
    mStudentModel->setRowCount(1);
    ui->tvStudent->setModel(mStudentModel);
    ui->tvStudent->selectRow(0);

    /// START PLANNING TREE
    mPlanningModel->setHorizontalHeaderLabels({tr("Nombre"), tr("Tipo"), tr("Ponderación")});
    mPlanningModel->setColumnCount(3);

    ui->treePlanning->setItemDelegateForColumn(1, new ComboBoxDelegate); // adding a ComboBox in the tree.
    ui->treePlanning->setItemDelegateForColumn(2, new SpinDelegate); // adding a DoubleSpin in the tree.
    ui->treePlanning->setModel(mPlanningModel);
    ui->treePlanning->expandAll();
    ui->treePlanning->resizeColumnToContents(0);
    ui->treePlanning->resizeColumnToContents(1);
}

void NewKlass::loadKlassTable()
{        
    QDir Dir(QDir::toNativeSeparators(QDir::homePath() + "/.KlassRoom"));
    QFile *Abrir;
    QStringList code, name;

    if (!Dir.exists()) {
        Dir.mkdir(QDir::toNativeSeparators(QDir::homePath() + "/.KlassRoom"));
    }

    Abrir = new QFile(QDir::toNativeSeparators(Dir.absolutePath() + "/KlassList.csv"));

    if (!Abrir->open(QIODevice::ReadOnly | QIODevice::Text)) {        
        return;
    }

    QTextStream leer(Abrir);

    while (!leer.atEnd()) {
        auto linea = leer.readLine().split(":");
        if (!linea.at(0).isEmpty()) {
            code.append(linea.at(0));
            name.append(linea.at(1));
        }
    }

    mKlassModel->setRowCount(code.length() + 1);

    for (int i = 0; i < code.length(); i++) {
        for (int j = 0; j < 2; j++) {
            QString Datos = (j == 0 ? code.at(i) : name.at(i));

            if (!mKlassModel->item(i, j)) {
                mKlassModel->setItem(i, j, new QStandardItem(Datos));
            }
            else {
                mKlassModel->item(i, j)->setText(Datos);
            }
        }
    }

    ui->tvKlass->setModel(mKlassModel);    
    ui->tvKlass->selectRow(0);

    Abrir->close();
}

void NewKlass::saveKlass()
{
    QDir Dir(QDir::toNativeSeparators(QDir::homePath() + "/.KlassRoom"));
    QFile *Guardar;

    if (!Dir.exists()) {
        Dir.mkdir(QDir::toNativeSeparators(QDir::homePath() + "/.KlassRoom"));
    }

    Guardar = new QFile(QDir::toNativeSeparators(Dir.absolutePath() + "/KlassList.csv"));

    if (!Guardar->open(QIODevice::WriteOnly | QIODevice::Text)) {
        return;
    }

    QTextStream leer(Guardar);

    for (int ROW = 0; ROW < mKlassModel->rowCount() - 1; ++ROW) {
        bool isEmpty = mKlassModel->item(ROW) == nullptr && mKlassModel->item(ROW)->text().isEmpty() &&
                       mKlassModel->item(ROW, 1) == nullptr && mKlassModel->item(ROW, 1)->text().isEmpty();
        if (!isEmpty) {
            leer << mKlassModel->item(ROW)->text() << ":" << mKlassModel->item(ROW, 1)->text() << endl;
        }
    }

    Guardar->flush();
    Guardar->close();
}

void NewKlass::on_btnOpen_clicked()
{
    QString nameFile = QFileDialog::getOpenFileName(this, tr("Nuevo archivo"), QDir::homePath(), tr("Archivo CSV (*.csv)"));
    QFile *File;
    QStringList name, id;

    if (nameFile.isEmpty()) {
        return;
    }

    File = new QFile(QDir::toNativeSeparators(nameFile));

    if (!File->open(QIODevice::ReadOnly | QIODevice::Text)) {
        QMessageBox::critical(this, tr("Error en archivo"), tr("Error: No se puede abrir el archivo") + File->errorString());
        return;
    }

    QTextStream Read(File);

    while (!Read.atEnd()) {
        auto linea = Read.readLine().split(":");
        if (!linea.at(0).isEmpty()) {
            id.append(linea.at(0));
            name.append(linea.at(1));
        }
    }

    mStudentModel->setRowCount(id.length() + 1);

    for (int i = 0; i < id.length(); i++) {
        for (int j = 0; j < 2; j++) {
            QString Datos = (j == 0 ? id.at(i) : name.at(i));

            if (!mStudentModel->item(i, j)) {
                mStudentModel->setItem(i, j, new QStandardItem(Datos));
            }
            else {
                mStudentModel->item(i, j)->setText(Datos);
            }
        }
    }

    ui->tvStudent->setModel(mStudentModel);
    ui->tvStudent->update();


    File->close();
}

void NewKlass::on_txtSearch_textChanged(const QString &changedText)
{
    QStandardItemModel *searchModel = new QStandardItemModel(this);    
    const int ROWS = mKlassModel->rowCount() - 1;

    // IF NOT SEARCHING
    if (changedText.isEmpty()) {
        ui->tvKlass->setModel(mKlassModel);
        ui->tvKlass->update();
        ui->tvKlass->setEditTriggers(QAbstractItemView::DoubleClicked | QAbstractItemView::EditKeyPressed);
        ui->tvKlass->selectRow(0);
        return;
    }

    searchModel->setHorizontalHeaderLabels({tr("Código"), tr("Nombre")});
    searchModel->setColumnCount(2);    

    for (int i = 0; i < ROWS; ++i) {
        bool noItem = mKlassModel->item(i, 0) == nullptr || mKlassModel->item(i, 0)->text().isEmpty() ||
                      mKlassModel->item(i, 1) == nullptr || mKlassModel->item(i, 1)->text().isEmpty();

        if (!noItem) {
            QString oCode = mKlassModel->item(i, 0)->text();
            QString oName = mKlassModel->item(i, 1)->text();

            if (oCode.contains(changedText, Qt::CaseInsensitive) || oName.contains(changedText, Qt::CaseInsensitive)) {
                searchModel->appendRow({new QStandardItem(oCode), new QStandardItem(oName)});
            }
        }
    }

    ui->tvKlass->setModel(searchModel);
    ui->tvKlass->update();
    ui->tvKlass->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tvKlass->selectRow(0);
}

void NewKlass::klassDataChanged(QStandardItem *item)
{
    int rowItem = item->row();
    int rowModel = mKlassModel->rowCount() - 1;

    if (rowItem == rowModel) {
        mKlassModel->appendRow(new QStandardItem);
    }

    ui->tvKlass->update();
}

void NewKlass::studentDataChanged(QStandardItem *item)
{
    int rowItem = item->row();
    int rowModel = mStudentModel->rowCount() - 1;

    if (rowItem == rowModel) {
        mStudentModel->appendRow(new QStandardItem);
    }

    ui->tvStudent->update();
}

void NewKlass::planningDataChanged(QStandardItem *item)
{        
    if (item->column() == 1){
        // TYPE/NAME COLUMN
        if (item->text() == tr("Acumulativo")) {
            item->parent()->child(item->row())->setIcon(QIcon(":/icons/020-notebook.png"));
        }
        else {
            item->parent()->child(item->row())->setIcon(QIcon(":/icons/040-test.png"));
        }
    }
    else if (item->column() == 2) {

    }
}

void NewKlass::on_BtnAdd_clicked()
{
    ui->BtnAdd->showMenu();
}

void NewKlass::on_actionAddPartial_triggered()
{
    QStandardItem *name = new QStandardItem(tr("Nuevo parcial"));
    QStandardItem *type = new QStandardItem;
    QStandardItem *pond = new QStandardItem("0");

    type->setEditable(false);    
    mPlanningModel->appendRow({name, type, pond});

    ui->treePlanning->resizeColumnToContents(0);
    ui->treePlanning->expandAll();
}

void NewKlass::on_actionAddWork_triggered()
{            
    QStandardItem *name, *type, *pond; // Item pointer for name, type and ponderation.
    QString nameType;
    QInputDialog dialog(this); // Dialog for select between work and test.
    int row = ui->treePlanning->currentIndex().parent().row(); // The row is child.

    /// GETTING TYPE FOR DIALOG
    dialog.setComboBoxItems({tr("Acumulativo"), tr("Examen")});
    dialog.setLabelText(tr("Seleccione un tipo"));

    if (!dialog.exec()) {
        return;
    }

    nameType = dialog.textValue();

    /// SETTING THE NAME AND ICON TYPE
    if (row == -1) {
        row = ui->treePlanning->currentIndex().row();
        if (row == -1) {
            QMessageBox::information(this, tr("Ningún parcial"), tr("No hay un parcial seleccionado o agregue uno."));
            return;
        }
    }

    if (nameType == "Acumulativo") {
        name = new QStandardItem(QIcon(":/icons/020-notebook.png"), tr("Nuevo ") + nameType.toLower()); // Work.
    }
    else {
        name = new QStandardItem(QIcon(":/icons/040-test.png"), tr("Nuevo ") + nameType.toLower()); // Test.
    }

    type = new QStandardItem(nameType); // Set the type.
    pond = new QStandardItem("0"); // Set the default ponderation.
    mPlanningModel->item(row, 0)->appendRow({name, type, pond}); // Insert in the model.

    /// UPDATING THE TREEVIEW
    ui->treePlanning->resizeColumnToContents(0);
    ui->treePlanning->resizeColumnToContents(1);
    ui->treePlanning->expandAll();
}

void NewKlass::on_BtnRemove_clicked()
{
    int Row = ui->treePlanning->currentIndex().row();

    if (ui->treePlanning->currentIndex().parent().row() != -1) {
        int parentRow = ui->treePlanning->currentIndex().parent().row();

        mPlanningModel->item(parentRow)->removeRow(Row);
    }
    else {
        if (Row == -1) {
            QMessageBox::information(this, tr("No hay selección"), tr("Seleccione una fila para eleminarla."));
            return;
        }

        mPlanningModel->removeRow(Row);
    }

    ui->treePlanning->update();
}
