#include "editstudent.h"
#include "ui_editstudent.h"

#include <QRegExpValidator>

EditStudent::EditStudent(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditStudent)
{
    ui->setupUi(this);
    QRegExp regExpId("[0-9]{11}");

    ui->txtId->setValidator(new QRegExpValidator(regExpId, this));    
}

EditStudent::EditStudent(QString id, QString name, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditStudent)
{
    ui->setupUi(this);

    QRegExp regExpId("[0-9]{11}");

    ui->txtId->setValidator(new QRegExpValidator(regExpId, this));    

    ui->txtId->setText(id);
    ui->txtName->setText(name);
}

EditStudent::~EditStudent()
{
    delete ui;
}

void EditStudent::setObs(double note)
{
    if (note >= 65) {
        ui->cbObs->addItem(tr("APROBADO"));
    }
    else if (note > 0) {
        ui->cbObs->addItems({tr("REPROBADO"), tr("ABANDONADO")});
    }
    else {
        ui->cbObs->addItem(tr("NO SE PRESENTÓ"));
    }
}

QString EditStudent::getName()
{
    return ui->txtName->text();
}

QString EditStudent::getId()
{
    return ui->txtId->text();
}

QString EditStudent::getObs()
{
    return ui->cbObs->currentText();
}

void EditStudent::on_buttonBox_accepted()
{
    accept();
}

void EditStudent::on_buttonBox_rejected()
{
    reject();
}
