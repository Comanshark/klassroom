#ifndef PANELSTUDENT_H
#define PANELSTUDENT_H

#include <QWidget>

class KlassProyect;
class QStandardItemModel;

namespace Ui {
class PanelStudent;
}

class PanelStudent : public QWidget
{
    Q_OBJECT

public:
    explicit PanelStudent(KlassProyect * const proyect, QWidget *parent = nullptr);
    ~PanelStudent();

private slots:
    void on_btnAddStudent_clicked();

    void on_btnRemoveStudent_clicked();

    void on_txtSearch_textChanged(const QString &arg1);

    void on_tvStudent_doubleClicked(const QModelIndex &index);

private:
    // PRIVATE METHODS MEMBERS
    void refreshTable();

    // PROPERTY MEMBERS
    Ui::PanelStudent *ui;
    KlassProyect *const mProyect;
    QStandardItemModel *mModel;
};

#endif // PANELSTUDENT_H
