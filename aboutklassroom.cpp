#include "aboutklassroom.h"
#include "ui_aboutklassroom.h"

#include <QMessageBox>
#include <QDesktopServices>
#include <QUrl>

AboutKlassRoom::AboutKlassRoom(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutKlassRoom)
{
    ui->setupUi(this);

    setWindowTitle(tr("Acerca de KlassRoom"));
}

AboutKlassRoom::~AboutKlassRoom()
{
    delete ui;
}

void AboutKlassRoom::on_buttonBox_rejected()
{
    rejected();
}

void AboutKlassRoom::on_btnAboutQt_clicked()
{
    QMessageBox::aboutQt(this);
}

void AboutKlassRoom::on_toolButton_clicked()
{
    QDesktopServices::openUrl(QUrl("mailto:carlos.riverunoz@gmail.com?subject=Comentarios sobre KlassRoom&body=Aquí el comentario..."));
}
