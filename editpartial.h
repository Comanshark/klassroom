#ifndef EDITPARTIAL_H
#define EDITPARTIAL_H

#include <QDialog>

namespace Ui {
class EditPartial;
}

class EditPartial : public QDialog
{
    Q_OBJECT

public:
    explicit EditPartial(QStringList partials, QWidget *parent = nullptr);
    EditPartial(QString name, QString desc, double pond, QWidget *parent = nullptr); // Edit constructor.
    ~EditPartial();

    void setTitlte(QString type);
    QString getName();
    QString getDesc();
    double getPond();
    int getPartial();
    int getType();

private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

    void on_cbType_currentIndexChanged(int index);

private:
    Ui::EditPartial *ui;
};

#endif // EDITPARTIAL_H
