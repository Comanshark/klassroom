#ifndef NEWKLASS_H
#define NEWKLASS_H

#include <QWidget>

class QStandardItemModel;
class QStandardItem;
class KlassProyect;

namespace Ui {
class NewKlass;
}

class NewKlass : public QWidget
{
    Q_OBJECT

public:
    explicit NewKlass(QWidget *parent = nullptr);

    ~NewKlass();

    bool createProyect(KlassProyect *const proyect);

private slots:
    void on_btnOpen_clicked();
    void on_txtSearch_textChanged(const QString &changedText);
    void klassDataChanged(QStandardItem *);
    void studentDataChanged(QStandardItem *);
    void planningDataChanged(QStandardItem *);
    void on_BtnAdd_clicked();
    void on_actionAddPartial_triggered();
    void on_actionAddWork_triggered();
    void on_BtnRemove_clicked();   

private:
    // METHODS MEMBERS
    void startTables();
    void loadKlassTable();
    void saveKlass();


    // PROPERTY MEMBERS
    Ui::NewKlass *ui;
    QStandardItemModel *mKlassModel;
    QStandardItemModel *mStudentModel;
    QStandardItemModel *mPlanningModel;
};

#endif // NEWKLASS_H
