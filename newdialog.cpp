#include "newdialog.h"
#include "ui_newdialog.h"

#include "newklass.h"
#include "klassproyect.h"

NewDialog::NewDialog(KlassProyect *const proyect, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewDialog),    
    mProyect(proyect),
    newklass(new NewKlass(this))

{
    ui->setupUi(this);

    ui->lwTemplate->addItem(new QListWidgetItem(QIcon(":/icons/005-assign.png"), tr("Nuevo")));

    ui->lwTemplate->setCurrentRow(0);
}

NewDialog::~NewDialog()
{
    delete ui;
}

void NewDialog::on_lwTemplate_currentRowChanged(int currentRow)
{
    ui->verticalLayout->removeWidget(ui->verticalLayout->widget());

    switch (currentRow) {
    case 0:
        ui->verticalLayout->insertWidget(0, newklass);
        newklass->show();
        break;
    default:
        newklass->hide();
    }
}

void NewDialog::on_buttonBox_accepted()
{
    if (!newklass->createProyect(mProyect)) {
        return;
    }

    accept();
}

void NewDialog::on_buttonBox_rejected()
{
    reject();
}
