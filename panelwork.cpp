#include "panelwork.h"
#include "ui_panelwork.h"

#include "klassproyect.h"
#include "klassdatatype.h"
#include "spindelegate.h"

#include <QStandardItemModel>
#include <QMessageBox>

PanelWork::PanelWork(KlassProyect * const proyect, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PanelWork),
    mProyect(proyect),
    mModel(new QStandardItemModel(this))
{
    ui->setupUi(this);

    if (mProyect->sizeStudents() < 1) {
        QMessageBox::information(this, tr("No hay estudiantes"), tr("No hay estudiantes a mostrar"));
        return;
    }

    loadTableView();

    // CONNECTIONS
    connect(mModel, SIGNAL(itemChanged(QStandardItem *)), this, SLOT(updateData(QStandardItem *)));
}

PanelWork::~PanelWork()
{
    delete ui;
}

void PanelWork::loadTableView()
{
    // CREATING COLUMNS
    HorizontalLabels << tr("No. de cuenta") << tr("Nombre");

    for (int i = 0; i < mProyect->sizeWorks(); i++) {
        if (mProyect->getWork(static_cast<uint>(i)).cStudent == mProyect->getStudent(0).cStudent)
            HorizontalLabels << mProyect->getWork(static_cast<uint>(i)).wName;
    }

    mModel->setColumnCount(HorizontalLabels.size());
    mModel->setHorizontalHeaderLabels(HorizontalLabels);

    // CREATING ROWS
    for (int i = 0; i < mProyect->sizeStudents(); ++i) {
        Student getStudent = mProyect->getStudent(static_cast<uint>(i));
        QList<QStandardItem *> Row;
        QStandardItem *Id = new QStandardItem(getStudent.sId);
        QStandardItem *Name = new QStandardItem(getStudent.sName);

        Id->setEditable(false);
        Name->setEditable(false);

        Row << Id << Name;

        for (int j = 0; j < mProyect->sizeWorks(); j++) {
            Work getWork = mProyect->getWork(static_cast<uint>(j));

            if (getWork.cStudent == getStudent.cStudent) {
                Row << new QStandardItem(QString::number(getWork.wNote));
            }
        }

        mModel->appendRow(Row);
    }

    ui->tvWork->setModel(mModel);
    ui->tvWork->resizeColumnsToContents();


    // CREATING SPINBOX DELEGATE
    for (int i = 2; i < HorizontalLabels.size(); ++i) {
        ui->tvWork->setItemDelegateForColumn(i, new SpinDelegate);
    }
}

void PanelWork::on_txtSearch_textChanged(const QString &changedText)
{
    QStandardItemModel *searchModel = new QStandardItemModel(this);
    const int ROWS = mModel->rowCount();

    // IF NOT SEARCHING
    if (changedText.isEmpty()) {
        ui->tvWork->setModel(mModel);
        ui->tvWork->update();
        disconnect(searchModel, SIGNAL(itemChanged(QStandardItem *)), this, SLOT(searchModelChanged(QStandardItem *)));
        return;
    }

    // COPY THE SAME STRUCTURE
    QStringList HorizontalLabels {tr("No. de cuenta"), tr("Nombre")};

    for (int i = 0; i < mProyect->sizeWorks(); i++) {
        if (mProyect->getWork(static_cast<uint>(i)).cStudent == mProyect->getStudent(0).cStudent)
            HorizontalLabels << mProyect->getWork(static_cast<uint>(i)).wName;
    }

    searchModel->setHorizontalHeaderLabels(HorizontalLabels);
    searchModel->setColumnCount(HorizontalLabels.size());

    // CREATE A COPY MODEL FOR THE SEARCH TEXT
    for (int i = 0; i < ROWS; ++i) {
        QString oId = mModel->item(i, 0)->text();
        QString oName = mModel->item(i, 1)->text();

        if (oId.contains(changedText, Qt::CaseInsensitive) || oName.contains(changedText, Qt::CaseInsensitive)) {
            QList<QStandardItem *> Items;
            Items.append(new QStandardItem(oId));
            Items.append(new QStandardItem(oName));
            for (int j = 2; j < mModel->columnCount(); ++j) {
                Items.append(new QStandardItem(QString::number(mModel->item(i, j)->data().toDouble())));
            }

            searchModel->appendRow(Items);
        }
    }

    // UPDATE THE TABLEVIEW
    connect(searchModel, SIGNAL(itemChanged(QStandardItem *)), this, SLOT(searchModelChanged(QStandardItem *)));
    ui->tvWork->setModel(searchModel);
    ui->tvWork->update();
}

void PanelWork::updateData(QStandardItem *item)
{
    double itemNote = item->text().toDouble();
    QString nameWork = HorizontalLabels.at(item->column());

    for (int WORKROWS = 0; WORKROWS < mProyect->sizeWorks(); ++WORKROWS) {
        Work getWork = mProyect->getWork(static_cast<uint>(WORKROWS));

        if (mProyect->getStudent(static_cast<uint>(item->row())).cStudent == getWork.cStudent && nameWork == getWork.wName) {
            mProyect->updateWorkNote(static_cast<uint>(WORKROWS), itemNote);
        }
    }
}

void PanelWork::searchModelChanged(QStandardItem *item)
{
    for (int i = 0; i < mModel->rowCount(); ++i) {
        if (mModel->item(i, 0)->text() == item->model()->item(item->row())->text()) {
            mModel->item(i, item->column())->setText(item->text());
            break;
        }
    }
}
