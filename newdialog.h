#ifndef NEWDIALOG_H
#define NEWDIALOG_H

#include <QDialog>

class NewKlass;
namespace Ui {
class NewDialog;
}
class KlassProyect;
class QAbstractButton;
class KlassProyect;

class NewDialog : public QDialog
{
    Q_OBJECT

public:
    explicit NewDialog(KlassProyect *const proyect, QWidget *parent = nullptr);
    ~NewDialog();

private slots:
    void on_lwTemplate_currentRowChanged(int currentRow);

    void on_buttonBox_accepted();        

    void on_buttonBox_rejected();

private:
    Ui::NewDialog *ui;
    KlassProyect *const mProyect;

    // UI FORMS
    NewKlass *newklass;
};

#endif // NEWDIALOG_H
