#include "spindelegate.h"

#include <QDoubleSpinBox>

SpinDelegate::SpinDelegate()
{

}

QWidget *SpinDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QDoubleSpinBox *newSpin = new QDoubleSpinBox(parent);

    newSpin->setRange(0.0, 100.0);

    (void) option;
    (void) index;

    return newSpin;
}

void SpinDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    QDoubleSpinBox *spin = static_cast<QDoubleSpinBox *>(editor);

    spin->setValue(index.data().toDouble());
}

void SpinDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    QDoubleSpinBox *spin = static_cast<QDoubleSpinBox *>(editor);

    model->setData(index, spin->value());
}

void SpinDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    editor->setGeometry(option.rect);

    (void) index;
}
