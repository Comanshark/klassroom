#ifndef ABOUTKLASSROOM_H
#define ABOUTKLASSROOM_H

#include <QDialog>

namespace Ui {
class AboutKlassRoom;
}

class AboutKlassRoom : public QDialog
{
    Q_OBJECT

public:
    explicit AboutKlassRoom(QWidget *parent = nullptr);
    ~AboutKlassRoom();

private slots:
    void on_buttonBox_rejected();

    void on_btnAboutQt_clicked();

    void on_toolButton_clicked();

private:
    Ui::AboutKlassRoom *ui;
};

#endif // ABOUTKLASSROOM_H
