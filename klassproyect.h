#ifndef KLASSPROYECT_H
#define KLASSPROYECT_H

#include "klassdatatype.h"

#include <QString>
#include <QVector>


class KlassProyect
{
public:
    explicit KlassProyect();
    KlassProyect(QString file);

    // SETTERS FUNCTIONS
    /// SET NAME ///
    /// \brief This function set the name proyect.
    /// \param newName
    ///
    void setName(QString newName, QString newCode, QString newSection);
    void setCode(QString newCode);
    void setSection(QString newSection);
    /// SET FILE ///
    /// \brief This function set the file path.
    /// \param newFile
    ///
    void setFile(QString newFile);
    void setStudent(QString name, QString id);
    void setPartial(QString name, QString desc, double pond);
    void setWork(uint cPartial, QString name, QString desc, double pond);
    void setTest (uint cPartial, QString name, QString desc, double pond);


    // UPDATE FUNCTIONS
    void updateStudent(uint cStudent, QString name, QString id);
    void updatePartial(uint cPartial, QString name, QString desc, double pond);
    void updateWork(uint cWork, QString name, QString desc, double pond);
    void updateTest(uint cTest, QString name, QString desc, double pond);
    void updateWorkNote(uint cWork, double note);
    void updateTestNote(uint cTest, double note);

    // REMOVE FUNCTIONS
    void removeStudent(uint student);
    void removePartial(uint partial);
    void removeWork(uint work);
    void removeTest(uint test);

    // GETTERS FUNCTION
    QString getName() const;
    QString getCode() const;
    QString getSection() const;
    QString getFile() const;
    Student getStudent(uint student) const;
    Partial getPartial(uint partial) const;
    Work getWork(uint work) const;
    Test getTest(uint test) const;

    int sizeStudents() const;
    int sizePartials() const;
    int sizeWorks() const;
    int sizeTests() const;

    // SAVE/LOAD PROYECT

    void saveProyect();
    void loadProyect();

private:
    // FUNCTIONS MEMBERS
    void clearMembers();

    // PROPERTY MEMBERS
    QString mName; // Proyect name.
    QString mCode; // Proyect code.
    QString mSection; // Proyect section.
    QString mFile; // File path name for proyect.
    QVector<Student> mStudent; // Array for Students.
    int lastStudent; // Last student code.
    QVector<Partial> mPartial; // Array for Partial.
    int lastPartial; // Last partial code.
    QVector<Work> mWork; // Arrray for Works.
    int lastWork; // Last work code.
    QVector<Test> mTest; // Array for Tests.
    int lastTest; // Last test code.

    QStringList mQuerys; // Array for Querys.
};

#endif // KLASSPROYECT_H
