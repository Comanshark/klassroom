#include "paneltest.h"
#include "ui_paneltest.h"

#include "klassproyect.h"
#include "spindelegate.h"

#include <QStandardItemModel>
#include <QMessageBox>

PanelTest::PanelTest(KlassProyect * const proyect, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PanelTest),
    mProyect(proyect),
    mModel(new QStandardItemModel(this))
{
    ui->setupUi(this);

    if (mProyect->sizeStudents() < 1) {
        QMessageBox::information(this, tr("No hay alumnos"), tr("No hay ningún alumno a evaluar"));
        return;
    }

    loadTableView();

    // CONNECTIONS
    connect(mModel, SIGNAL(itemChanged(QStandardItem *)), this, SLOT(noteUpdate(QStandardItem *)))
            ;
}

PanelTest::~PanelTest()
{
    delete ui;
}

void PanelTest::loadTableView()
{
    // CREATING COLUMNS
    HorizontalLabels << tr("No. de cuenta") << tr("Nombre");

    for (int i = 0; i < mProyect->sizeTests(); i++) {
        if (mProyect->getTest(static_cast<uint>(i)).cStudent == mProyect->getStudent(0).cStudent)
            HorizontalLabels << mProyect->getTest(static_cast<uint>(i)).tName;
    }

    mModel->setColumnCount(HorizontalLabels.size());
    mModel->setHorizontalHeaderLabels(HorizontalLabels);

    // CREATING ROWS
    for (int i = 0; i < mProyect->sizeStudents(); ++i) {
        Student getStudent = mProyect->getStudent(static_cast<uint>(i));
        QList<QStandardItem *> Row;
        QStandardItem *Id = new QStandardItem(getStudent.sId);
        QStandardItem *Name = new QStandardItem(getStudent.sName);

        Id->setEditable(false);
        Name->setEditable(false);

        Row << Id << Name;

        for (int j = 0; j < mProyect->sizeTests(); j++) {
            Test getTest = mProyect->getTest(static_cast<uint>(j));

            if (getTest.cStudent == getStudent.cStudent) {
                Row << new QStandardItem(QString::number(getTest.tNote));
            }
        }

        mModel->appendRow(Row);
    }

    ui->tvTest->setModel(mModel);
    ui->tvTest->resizeColumnsToContents();


    // CREATING SPINBOX DELEGATE
    for (int i = 2; i < HorizontalLabels.size(); ++i) {
        ui->tvTest->setItemDelegateForColumn(i, new SpinDelegate);
    }
}

void PanelTest::on_txtSearch_textChanged(const QString &changedText)
{
    QStandardItemModel *searchModel = new QStandardItemModel(this);
    const int ROWS = mModel->rowCount();

    // IF NOT SEARCHING
    if (changedText.isEmpty()) {
        ui->tvTest->setModel(mModel);
        ui->tvTest->update();
        disconnect(searchModel, SIGNAL(itemChanged(QStandardItem *)), this, SLOT(searchModelChanged(QStandardItem *)));
        return;
    }

    // COPY THE SAME STRUCTURE
    QStringList HorizontalLabels {tr("No. de cuenta"), tr("Nombre")};

    for (int i = 0; i < mProyect->sizeTests(); i += mProyect->sizeStudents()) {
        if (mProyect->getTest(static_cast<uint>(i)).cStudent == mProyect->getStudent(0).cStudent)
            HorizontalLabels << mProyect->getTest(static_cast<uint>(i)).tName;
    }

    searchModel->setHorizontalHeaderLabels(HorizontalLabels);
    searchModel->setColumnCount(HorizontalLabels.size());

    // CREATE A COPY MODEL FOR THE SEARCH TEXT
    for (int i = 0; i < ROWS; ++i) {
        QString oId = mModel->item(i, 0)->text();
        QString oName = mModel->item(i, 1)->text();

        if (oId.contains(changedText, Qt::CaseInsensitive) || oName.contains(changedText, Qt::CaseInsensitive)) {
            QList<QStandardItem *> Items;
            Items.append(new QStandardItem(oId));
            Items.append(new QStandardItem(oName));
            for (int j = 2; j < mModel->columnCount(); ++j) {
                Items.append(new QStandardItem(mModel->item(i, j)->text()));
            }

            searchModel->appendRow(Items);
        }
    }

    // UPDATE THE TABLEVIEW
    connect(searchModel, SIGNAL(itemChanged(QStandardItem *)), this, SLOT(searchModelChanged(QStandardItem *)));
    ui->tvTest->setModel(searchModel);
    ui->tvTest->update();
}

void PanelTest::noteUpdate(QStandardItem *item)
{
    double itemNote = item->text().toDouble();
    QString nameItem = HorizontalLabels.at(item->column());

    for (int WORKROWS = 0; WORKROWS < mProyect->sizeTests(); ++WORKROWS) {
        Test getTest = mProyect->getTest(static_cast<uint>(WORKROWS));

        if (mProyect->getStudent(static_cast<uint>(item->row())).cStudent == getTest.cStudent && nameItem == getTest.tName) {
            mProyect->updateTestNote(static_cast<uint>(WORKROWS), itemNote);
        }
    }
}

void PanelTest::searchModelChanged(QStandardItem *item)
{
    // SEARCH THE ITEM FOR THE MODEL
    for (int i = 0; i < mModel->rowCount(); ++i) {
        if (mModel->item(i, 0)->text() == item->model()->item(item->row())->text()) {
            mModel->item(i, item->column())->setText(item->text());
            break;
        }
    }
}
