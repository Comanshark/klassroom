#include "panelpartial.h"
#include "ui_panelpartial.h"

#include "klassproyect.h"
#include "klassdatatype.h"
#include "editpartial.h"

#include <QStandardItemModel>
#include <QMessageBox>

PanelPartial::PanelPartial(KlassProyect * const proyect, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PanelPartial),
    mModel(new QStandardItemModel(this)),
    mProyect(proyect)
{
    ui->setupUi(this);

    if (mProyect->sizeStudents() < 1) {
        QMessageBox::information(this, tr("No hay alumnos"), tr("No hay alumnos agregados. Agregue para habilitar la edicion de parciales"));
        return;
    }

    loadTreeView();
}

PanelPartial::~PanelPartial()
{
    delete ui;
}

void PanelPartial::loadTreeView()
{
    // PREPARING COLUMNS
    mModel->setHorizontalHeaderLabels({tr("Nombre"), tr("Tipo"), tr("Ponderación")});

    // SETTING THE PARTIALS, WORKS AND TESTS
    for (int i = 0; i < mProyect->sizePartials(); ++i) {
        Partial getPartial = mProyect->getPartial(static_cast<uint>(i));

        QStandardItem *name = new QStandardItem(getPartial.pName);
        QStandardItem *type = new QStandardItem;
        QStandardItem *pond = new QStandardItem(QString::number(getPartial.pPond));

        type->setEditable(false);
        mModel->appendRow({name, type, pond});

        // ADD WORKS IN THE CURRENT PARTIAL
        for (int j = 0; j < mProyect->sizeWorks(); j++) {
            Work getWork = mProyect->getWork(static_cast<uint>(j));

            if (getPartial.cPartial == getWork.cPartial && mProyect->getStudent(0).cStudent == getWork.cStudent) {
                QStandardItem *name = new QStandardItem(QIcon(":/icons/020-notebook.png"), getWork.wName);
                QStandardItem *type = new QStandardItem(tr("Acumulativo"));
                QStandardItem *pond = new QStandardItem(QString::number(getWork.wPond));

                type->setEditable(false);

                mModel->item(i)->appendRow({name, type, pond});
            }
        }

        // ADD TEST IN THE CURRENT PARTIAL
        for (int j = 0; j < mProyect->sizeTests(); j++) {
            Test getTest = mProyect->getTest(static_cast<uint>(j));

            if (getPartial.cPartial == getTest.cPartial && mProyect->getStudent(0).cStudent == getTest.cStudent) {
                QStandardItem *name = new QStandardItem(QIcon(":/icons/040-test.png"), getTest.tName);
                QStandardItem *type = new QStandardItem(tr("Examen"));
                QStandardItem *pond = new QStandardItem(QString::number(getTest.tPond));

                type->setEditable(false);

                mModel->item(i)->appendRow({name, type, pond});
            }
        }
    }

    // ADD TO TREEVIEW
    ui->tvPartial->setModel(mModel);
    ui->tvPartial->expandAll();
    ui->tvPartial->resizeColumnToContents(0);
}

void PanelPartial::on_btnAddPartial_clicked()
{
    QStringList partials;

    for (int i = 0; i < mProyect->sizePartials(); ++i) {
        partials << mProyect->getPartial(static_cast<uint>(i)).pName;
    }

    EditPartial editor(partials, this);

    if (editor.exec() == QDialog::Rejected) {
        return;
    }

    QStandardItem *name; // Name item.
    QStandardItem *type; // Type item.
    QStandardItem *pond; // Ponderation item.

    if (editor.getType() == 0) {
        name = new QStandardItem(editor.getName());
        type = new QStandardItem;
        pond = new QStandardItem(QString::number(editor.getPond()));

        mModel->appendRow({name, type, pond});
        mProyect->setPartial(editor.getName(), editor.getDesc(), editor.getPond());
    }
    else if (editor.getType() == 1) {
        name = new QStandardItem(QIcon(":/icons/020-notebook.png"), editor.getName());
        type = new QStandardItem(tr("Acumulativo"));
        pond = new QStandardItem(QString::number(editor.getPond()));

        mModel->item(editor.getPartial())->appendRow({name, type, pond});
        mProyect->setWork(static_cast<uint>(editor.getPartial()), editor.getName(), editor.getDesc(), editor.getPond());
    }
    else {
        name = new QStandardItem(QIcon(":/icons/040-test.png"), editor.getName());
        type = new QStandardItem(tr("Examen"));
        pond = new QStandardItem(QString::number(editor.getPond()));

        mModel->item(editor.getPartial())->appendRow({name, type, pond});
        mProyect->setTest(static_cast<uint>(editor.getPartial()), editor.getName(), editor.getDesc(), editor.getPond());
    }

    ui->tvPartial->expandAll();
}

void PanelPartial::on_btnRemovePartial_clicked()
{
    int Row = ui->tvPartial->currentIndex().row();

    if (ui->tvPartial->currentIndex().parent().row() != -1) { // If the index is a child.
        int parentRow = ui->tvPartial->currentIndex().parent().row();        
        QString nameModel = mModel->item(parentRow)->child(Row)->text();
        double pondModel = mModel->item(parentRow)->child(Row, 2)->text().toDouble();

        if (mModel->item(parentRow)->child(Row, 1)->text() == tr("Acumulativo")) {
            for (int i = 0; i < mProyect->sizeWorks(); i += mProyect->sizeStudents()) {

                if (nameModel == mProyect->getWork(static_cast<uint>(i)).wName && pondModel == mProyect->getWork(static_cast<uint>(i)).wPond) {
                    mProyect->removeWork(static_cast<uint>(i));
                    break;
                }
            }
        }
        else {
            for (int i = 0; i < mProyect->sizeWorks(); i += mProyect->sizeStudents()) {
                if (nameModel == mProyect->getTest(static_cast<uint>(i)).tName && pondModel == mProyect->getTest(static_cast<uint>(i)).tPond) {
                    mProyect->removeTest(static_cast<uint>(i));
                    break;
                }
            }
        }

        mModel->item(parentRow)->removeRow(Row);
    }
    else { // If the index is parent
        if (Row == -1) {
            QMessageBox::information(this, tr("No hay selección"), tr("Seleccione una fila para eleminarla."));
            return;
        }

        mModel->removeRow(Row);
        mProyect->removePartial(static_cast<uint>(Row));
    }
}

void PanelPartial::on_tvPartial_doubleClicked(const QModelIndex &index)
{
    const int ROWSELECT = index.row();
    QString name, desc;
    double pond;

    // IF NOT SELECTED}
    if (ROWSELECT == -1) {
        QMessageBox::information(this, tr("Ninguna selección"), tr("No ha seleccionado ningún alumno."));
        return;
    }



    if (index.parent().row() == -1) {
        name = mModel->item(ROWSELECT)->text();
        desc = mProyect->getPartial(static_cast<uint>(ROWSELECT)).pDesc;
        pond = mModel->item(ROWSELECT, 2)->text().toDouble();

        EditPartial editor(name, desc, pond, this);
        editor.setWindowTitle(tr("Editar parcial"));
        editor.setTitlte(tr("parcial"));


        if (editor.exec() == QDialog::Rejected) return;

        name = editor.getName();
        desc = editor.getDesc();
        pond = editor.getPond();

        mModel->item(ROWSELECT)->setText(name);
        mProyect->updatePartial(static_cast<uint>(ROWSELECT), name, desc, pond);
        mModel->item(ROWSELECT, 2)->setText(QString::number(pond));
    }
    else {
        const int ROWPARENT = index.parent().row();

        name = mModel->item(ROWPARENT)->child(ROWSELECT)->text();        
        pond = mModel->item(ROWPARENT)->child(ROWSELECT, 2)->text().toDouble();
        QString type = mModel->item(ROWPARENT)->child(ROWSELECT, 1)->text();

        if (type == tr("Acumulativo")) {
            for (int i = 0; i < mProyect->sizeWorks(); i++) {
                QString wName = mProyect->getWork(static_cast<uint>(i)).wName;
                double wPond = mProyect->getWork(static_cast<uint>(i)).wPond;
                desc = mProyect->getWork(static_cast<uint>(i)).wDesc;

                if (name == wName && pond == wPond) {
                    EditPartial editor(name, desc, pond, this);
                    editor.setWindowTitle(tr("Editar acumulativo"));
                    editor.setTitlte(tr("acumulativo"));

                    if (editor.exec() == QDialog::Rejected) return;

                    name = editor.getName();
                    desc = editor.getDesc();
                    pond = editor.getPond();

                    mModel->item(ROWPARENT)->child(ROWSELECT)->setText(name);
                    mModel->item(ROWPARENT)->child(ROWSELECT, 2)->setText(QString::number(pond));
                    mProyect->updateWork(static_cast<uint>(i), name, desc, pond);
                    break;
                }
            }
        }
        else {
            for (int i = 0; i < mProyect->sizeTests(); i++) {
                QString tName = mProyect->getTest(static_cast<uint>(i)).tName;
                double tPond = mProyect->getTest(static_cast<uint>(i)).tPond;
                desc = mProyect->getTest(static_cast<uint>(i)).tDesc;

                if (name == tName && pond == tPond) {
                    EditPartial editor(name, desc, pond, this);
                    editor.setWindowTitle(tr("Editar examen"));
                    editor.setTitlte(tr("examen"));

                    if (editor.exec() == QDialog::Rejected) return;

                    name = editor.getName();
                    desc = editor.getDesc();
                    pond = editor.getPond();

                    mModel->item(ROWPARENT)->child(ROWSELECT)->setText(name);
                    mModel->item(ROWPARENT)->child(ROWSELECT, 2)->setText(QString::number(pond));
                    mProyect->updateTest(static_cast<uint>(i), name, desc, pond);
                    break;
                }
            }
        }
    }

}
