#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;

    a.setApplicationName("KlassRoom");
    a.setApplicationVersion("0.1");
    a.setDesktopFileName("KlassRoom");
    a.setOrganizationName(QObject::tr("KDesarr"));
    w.show();

    return a.exec();
}
