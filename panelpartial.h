#ifndef PANELPARTIAL_H
#define PANELPARTIAL_H

#include <QWidget>

class KlassProyect;
class QStandardItemModel;

namespace Ui {
class PanelPartial;
}

class PanelPartial : public QWidget
{
    Q_OBJECT

public:
    explicit PanelPartial(KlassProyect * const proyect, QWidget *parent = nullptr);
    ~PanelPartial();

private slots:
    void on_btnAddPartial_clicked();

    void on_btnRemovePartial_clicked();

    void on_tvPartial_doubleClicked(const QModelIndex &index);

private:
    // METHODS MEMBERS
    void loadTreeView();

    // PROPERTY MEMBERS
    Ui::PanelPartial *ui;
    QStandardItemModel *mModel;
    KlassProyect * const mProyect;
};

#endif // PANELPARTIAL_H
