#include "startklass.h"
#include "ui_startklass.h"

StartKlass::StartKlass(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::StartKlass)
{
    ui->setupUi(this);

    connect(ui->BtnNew, SIGNAL(clicked()), this, SIGNAL(newButtonClicked()));
    connect(ui->BtnOpen, SIGNAL(clicked()), this, SIGNAL(openButtonClicked()));
}

StartKlass::~StartKlass()
{
    delete ui;
}
