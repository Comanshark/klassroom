#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "startklass.h"
#include "panelstudent.h"
#include "panelpartial.h"
#include "panelwork.h"
#include "paneltest.h"
#include "newdialog.h"
#include "aboutklassroom.h"

#include <QFileDialog>
#include <QGridLayout>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);    

    setWindowTitle("KlassRoom");    

    on_actionFrist_triggered();

    // DISABLING TOOLBAR

    ui->actionTest->setEnabled(false);
    ui->actionWork->setEnabled(false);
    ui->actionPartial->setEnabled(false);
    ui->actionStudent->setEnabled(false);
    ui->actionFrist->setChecked(true);

    ui->actionSave->setEnabled(false);
    ui->actionSaveAs->setEnabled(false);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setChecked(int checked)
{
    if (checked != STUDENT) ui->actionStudent->setChecked(false);
    if (checked != PARTIAL) ui->actionPartial->setChecked(false);
    if (checked != WORK) ui->actionWork->setChecked(false);
    if (checked != TEST) ui->actionTest->setChecked(false);
    if (checked != HELP) ui->actionHelp->setChecked(false);
    if (checked != START) ui->actionFrist->setChecked(false);
}

void MainWindow::on_actionNew_triggered()
{
    KlassProyect proyect;
    NewDialog *newKlass = new NewDialog(&proyect, this);

    if (newKlass->exec() == QDialog::Rejected) {
        return;
    }

    Proyect = new KlassProyect(proyect);

    setWindowTitle(Proyect->getName() + " " + tr("Sección ") + Proyect->getSection() + " | KlassRoom");
    setStartEnabled(false);
    on_actionStudent_triggered();
}

void MainWindow::on_actionOpen_triggered()
{
    QString nameFile = QFileDialog::getOpenFileName(this, tr("Abrir proyecto"), QDir::homePath(), tr("Archivo de KlassRoom (*.klass)"));

    if (nameFile.isEmpty()) return;

    Proyect = new KlassProyect;
    Proyect->setFile(nameFile);
    Proyect->loadProyect();
    setWindowTitle(Proyect->getName() + " " + tr("Sección ") + Proyect->getSection() + " | KlassRoom");

    setStartEnabled(false);
    on_actionStudent_triggered();
}

void MainWindow::on_actionSave_triggered()
{
    if (Proyect->getFile().isEmpty()) {
        QString file = QFileDialog::getSaveFileName(this, tr("Guardar proyecto"), QDir::homePath(), tr("Archivo de KlassRoom (*.klass)"));
        if (file.isEmpty()) return;

        Proyect->setFile(file);
        Proyect->saveProyect();
    }
    else {
        Proyect->saveProyect();
    }
}

void MainWindow::on_actionSaveAs_triggered()
{
    QString file = QFileDialog::getSaveFileName(this, tr("Guardar como..."), QDir::homePath(), tr("Archivo de KlassRoom (*.klass)"));
    if (file.isEmpty()) return;

    Proyect->setFile(file);
    Proyect->saveProyect();
}

void MainWindow::on_actionStudent_triggered()
{
    PanelStudent *panel = new PanelStudent(Proyect, this);

    // SETTING UP THE WIDGET
    delete centralWidget();
    setCentralWidget(panel);

    // CHECKED CONTROL
    if (!ui->actionStudent->isChecked()) ui->actionStudent->setChecked(true);
    setChecked(STUDENT);
}

void MainWindow::on_actionPartial_triggered()
{
    PanelPartial *panel = new PanelPartial(Proyect, this);

    // SETTING UP THE WIDGET
    delete centralWidget();
    setCentralWidget(panel);

    // CHECKED CONTROL
    if (!ui->actionPartial->isChecked()) ui->actionPartial->setChecked(true);
    setChecked(PARTIAL);
}

void MainWindow::on_actionWork_triggered()
{
    PanelWork *panel = new PanelWork(Proyect, this);

    // SETTING UP THE WIDGET
    delete centralWidget();
    setCentralWidget(panel);

    // CHECKED CONTROL
    if (!ui->actionWork->isChecked()) ui->actionWork->setChecked(true);
    setChecked(WORK);
}

void MainWindow::on_actionTest_triggered()
{
    PanelTest *panel = new PanelTest(Proyect, this);

    // SETTING UP THE WIDGET
    delete centralWidget();
    setCentralWidget(panel);

    // CHECKED CONTROL
    if (!ui->actionTest->isChecked()) ui->actionTest->setChecked(true);
    setChecked(TEST);
}

void MainWindow::on_actionHelp_triggered()
{
    if (!ui->actionHelp->isChecked()) ui->actionHelp->setChecked(true);
    setChecked(HELP);
}

void MainWindow::on_actionQuit_triggered()
{
    close();
}

void MainWindow::on_actionAbout_triggered()
{
    AboutKlassRoom *about = new AboutKlassRoom(this);

    about->exec();

    delete about;
}

void MainWindow::on_actionFrist_triggered()
{
    if (centralWidget() != nullptr) {
        delete centralWidget();
    }

    setStartEnabled(true);

    if (!ui->actionFrist->isChecked()) ui->actionFrist->setChecked(true);
    setChecked(START);
}
