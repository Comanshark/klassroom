#include "klassproyect.h"

#include <QDebug>
#include <QFile>
#include <QDir>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>

KlassProyect::KlassProyect()
    : mName(""),
      mFile(""),
      lastStudent(1),
      lastPartial(1),
      lastWork(1),
      lastTest(1)
{

}

KlassProyect::KlassProyect(QString file)
    : mFile(file)
{    
}


void KlassProyect::setName(QString newName, QString newCode, QString newSection)
{
    if (mName == newName) return;
    if (mCode == newCode) return;
    if (mSection == newSection) return;

    mName = newName;
    mCode = newCode;
    mSection = newSection;

    mQuerys << QString("INSERT INTO datafile (name_file, code_file, sect_file, last_student, last_partial, last_work, last_test) VALUES ('%1', '%2', '%3', %4, %5, %6, %7);")
               .arg(newName).arg(newCode).arg(newSection).arg(1).arg(1).arg(1).arg(1);
}

void KlassProyect::setFile(QString newFile)
{
    if (mFile == newFile) return;

    mFile = newFile;
}

void KlassProyect::setStudent(QString name, QString id)
{
    Student addStudent;

    addStudent.cStudent = (mStudent.isEmpty() ? 1 : static_cast<uint>(lastStudent + 1));
    addStudent.sName = name;
    addStudent.sId = id;

    mStudent << addStudent;
    lastStudent = static_cast<int>(addStudent.cStudent);

    mQuerys << QString("INSERT INTO students (name_student, id_student) VALUES ('%1', '%2');").arg(name).arg(id);

    // IF EXIST A ONE WORK
    if (!mWork.isEmpty()) {
        QVector<Work> newWorks;

        for (int i = 0; i < mWork.size(); ++i) {
            if (mWork.at(i).cStudent == mStudent.first().cStudent) {
                Work addWork = mWork.at(i);
                addWork.cStudent = addStudent.cStudent;
                addWork.cWork = static_cast<uint>(++lastWork);
                addWork.wNote = 0;

                newWorks << addWork;
            }
        }

        for (int i = 0; i < newWorks.size(); ++i) {
            mWork << newWorks.at(i);            

            mQuerys << QString("INSERT INTO works (cod_student, cod_partial, name_work, desc_work, pond_work, note_work) "
                               "VALUES (%1, %2, '%3', '%4', %5, %6);")
                       .arg(newWorks.at(i).cStudent).arg(newWorks.at(i).cPartial)
                       .arg(newWorks.at(i).wName).arg(newWorks.at(i).wDesc).arg(newWorks.at(i).wPond).arg(0);
        }
    }

    // IF EXIST A ONE TEST
    if (!mTest.isEmpty()) {
        QVector<Test> newTests;

        for (int i = 0; i < mTest.size(); ++i) {
            if (mTest.at(i).cStudent == mStudent.first().cStudent) {
                Test addTest = mTest.at(i);
                addTest.cStudent = addStudent.cStudent;
                addTest.cTest = static_cast<uint>(++lastTest);
                addTest.tNote = 0;

                newTests << addTest;
            }
        }

        for (int i = 0; i < newTests.size(); ++i) {
            mTest << newTests.at(i);            

            mQuerys << QString("INSERT INTO tests (cod_student, cod_partial, name_test, desc_test, pond_test, note_test) "
                               "VALUES (%1, %2, '%3', '%4', %5, %6);")
                       .arg(newTests.at(i).cStudent).arg(newTests.at(i).cPartial)
                       .arg(newTests.at(i).tName).arg(newTests.at(i).tDesc).arg(newTests.at(i).tPond).arg(0);
        }
    }
}

void KlassProyect::setPartial(QString name, QString desc, double pond)
{
    Partial addPartial;

    addPartial.cPartial = (mPartial.isEmpty() ? 1 : static_cast<uint>(++lastPartial));
    addPartial.pName = name;
    addPartial.pDesc = desc;
    addPartial.pPond = pond;

    mPartial << addPartial;    

    mQuerys << QString("INSERT INTO partials (name_partial, desc_partial, pond_partial) "
                       "VALUES ('%1', '%2', %3);").arg(name).arg(desc).arg(pond);
}

void KlassProyect::setWork(uint cPartial, QString name, QString desc, double pond)
{
    if (mPartial.isEmpty()) {
        qDebug() << QObject::tr("Error: no hay un parcial al cual añadir un acumulativo.");
        return;
    }

    if (mStudent.isEmpty()) {
        qDebug() << QObject::tr("Error: no hay un estudiante al cual añadir un acumulativo.");
        return;
    }

    for (int i = 0; i < mStudent.size(); ++i) {
        Work addWork;

        addWork.cWork = (mWork.isEmpty() ? 1 : static_cast<uint>(++lastWork));
        addWork.cStudent = mStudent.at(i).cStudent;
        addWork.cPartial = mPartial.at(static_cast<int>(cPartial)).cPartial;
        addWork.wName = name;
        addWork.wDesc = desc;
        addWork.wPond = pond;
        addWork.wNote = 0;

        mWork << addWork;        

        mQuerys << QString("INSERT INTO works (cod_student, cod_partial, name_work, desc_work, pond_work, note_work) "
                           "VALUES (%1, %2, '%3', '%4', %5, %6);").arg(addWork.cStudent).arg(addWork.cPartial).arg(name).arg(desc).arg(pond).arg(0);
    }
}

void KlassProyect::setTest(uint cPartial, QString name, QString desc, double pond)
{
    if (mPartial.isEmpty()) {
        qDebug() << QObject::tr("Error: no hay un parcial al cual añadir un acumulativo.");
        return;
    }

    if (mStudent.isEmpty()) {
        qDebug() << QObject::tr("Error: no hay un estudiante al cual añadir un acumulativo");
        return;
    }

    for (int i = 0; i < mStudent.size(); ++i) {
        Test addTest;

        addTest.cTest = (mTest.isEmpty() ? 1 : static_cast<uint>(++lastTest));
        addTest.cStudent = mStudent.at(i).cStudent;
        addTest.cPartial = mPartial.at(static_cast<int>(cPartial)).cPartial;
        addTest.tName = name;
        addTest.tDesc = desc;
        addTest.tPond = pond;
        addTest.tNote = 0;

        mTest << addTest;        

        mQuerys << QString("INSERT INTO tests (cod_student, cod_partial, name_test, desc_test, pond_test, note_test) "
                           "VALUES (%1, %2, '%3', '%4', %5, %6);")
                   .arg(addTest.cStudent).arg(addTest.cPartial).arg(name).arg(desc).arg(pond).arg(0);
    }
}

void KlassProyect::updateStudent(uint cStudent, QString name, QString id)
{
    int index = static_cast<int>(cStudent);

    if (mStudent.isEmpty() || mStudent.size() < index) {
        qDebug() << QObject::tr("Error: no existe el estudiante a editar.");
        return;
    }

    Student uStudent = mStudent.at(index);

    uStudent.sName = name;
    uStudent.sId = id;

    mStudent.replace(index, uStudent);

    mQuerys << QString("UPDATE students SET name_student = '%1', id_student = '%2' ").arg(name).arg(id) +
               QString("WHERE cod_student = %1;").arg(uStudent.cStudent);
}

void KlassProyect::updatePartial(uint cPartial, QString name, QString desc, double pond)
{
    int index = static_cast<int>(cPartial);

    if (mPartial.isEmpty() || mPartial.size() < index) {
        qDebug() << QObject::tr("Error: no existe el parcial a editar.");
        return;
    }

    Partial uPartial = mPartial.at(index);

    uPartial.pName = name;
    uPartial.pDesc = desc;
    uPartial.pPond = pond;

    mPartial.replace(index, uPartial);

    mQuerys << QString("UPDATE partials SET name_partial = '%1', desc_partial = '%2', pond_partial = %3 ").
               arg(name).arg(desc).arg(pond) + QString("WHERE cod_partial = %1;").arg(uPartial.cPartial);
}

void KlassProyect::updateWork(uint cWork, QString name, QString desc, double pond)
{
    int index = static_cast<int>(cWork);

    if (mWork.isEmpty() || mWork.size() < index) {
        qDebug() << QObject::tr("Error: no existe el trabajo a editar.");
        return;
    }

    QString nameWork = mWork.at(index).wName;
    QString descWork = mWork.at(index).wDesc;
    double pondWork = mWork.at(index).wPond;

    // Editing all works for student.
    for (int i = 0; i < mWork.size(); ++i) {
        Work uWork = mWork.at(i);
        if (uWork.wName == nameWork && uWork.wDesc == descWork && uWork.wPond == pondWork) {
            uWork.wName = name;
            uWork.wDesc = desc;
            uWork.wPond = pond;

            mWork.replace(i, uWork);

            mQuerys << QString("UPDATE works SET name_work = '%1', desc_work = '%2', pond_work = %3 ")
                       .arg(name).arg(desc).arg(pond) +
                       QString("WHERE cod_work = %1;").arg(uWork.cWork);
        }
    }
}

void KlassProyect::updateTest(uint cTest, QString name, QString desc, double pond)
{
    int index = static_cast<int>(cTest);

    if (mWork.isEmpty() || mTest.size() < index) {
        qDebug() << QObject::tr("Error: no existe el trabajo a editar.");
        return;
    }

    QString nameTest = mTest.at(index).tName;
    QString descTest = mTest.at(index).tDesc;
    double pondTest = mTest.at(index).tPond;

    // Editing all tests for student.
    for (int i = 0; i < mTest.size(); ++i) {
        Test uTest = mTest.at(i);
        if (uTest.tName == nameTest && uTest.tDesc == descTest && uTest.tPond == pondTest) {
            uTest.tName = name;
            uTest.tDesc = desc;
            uTest.tPond = pond;

            mTest.replace(i, uTest);

            mQuerys << QString("UPDATE tests SET name_test = '%1', desc_test = '%2', pond_test = %3 ")
                       .arg(name).arg(desc).arg(pond) +
                       QString("WHERE cod_test = %1;").arg(uTest.cTest);
        }
    }
}

void KlassProyect::updateWorkNote(uint cWork, double note)
{
    int index = static_cast<int>(cWork);

    if (mWork.isEmpty() || mWork.size() < index) {
        qDebug() << QObject::tr("Error: no existe el acumulativo a editar la nota.");
        return;
    }

    Work uWork = mWork.at(index);

    uWork.wNote = note;
    mWork.replace(index, uWork);

    mQuerys << QString("UPDATE works SET note_work = %1 WHERE cod_work = %2").arg(note).arg(uWork.cWork);
}

void KlassProyect::updateTestNote(uint cTest, double note)
{
    int index = static_cast<int>(cTest);

    if (mTest.isEmpty() || mTest.size() < index) {
        qDebug() << QObject::tr("Error: no existe el examen a editar la nota.");
        return;
    }

    Test uTest = mTest.at(index);

    uTest.tNote = note;
    mTest.replace(index, uTest);

    mQuerys << QString("UPDATE tests SET note_test = %1 WHERE cod_test = %2").arg(note).arg(uTest.cTest);
}

void KlassProyect::removeStudent(uint student)
{
    if (mStudent.isEmpty() || mStudent.size() < static_cast<int>(student)) {
        qDebug() << QObject::tr("Error: no existe el estudiante a eliminar.") << endl;
        return;
    }

    uint index = mStudent.at(static_cast<int>(student)).cStudent;

    // REMOVE WORKS    
    for (int i = 0; i < mWork.size(); ++i) {        
        if (index == mWork.at(i).cStudent) {
            mWork.removeAt(i);
            i--;
        }
    }

    mQuerys << QString("DELETE FROM works WHERE cod_student = %1;").arg(index);

    // REMOVE TESTS
    for (int i = 0; i < mTest.size(); ++i) {
        if (index == mTest.at(i).cStudent) {
            mTest.removeAt(i);
            i--;
        }
    }

    mQuerys << QString("DELETE FROM tests WHERE cod_student = %1;").arg(index);

    // REMOVE STUDENT
    mStudent.removeAt(static_cast<int>(student));

    mQuerys << QString("DELETE FROM students WHERE cod_student = %1;").arg(index);
}

void KlassProyect::removePartial(uint partial)
{
    if (mPartial.isEmpty() || mPartial.size() < static_cast<int>(partial)) {
        qDebug() << QObject::tr("Error: no existe el parcial a elminiar.") << endl;
        return;
    }

    uint index = mPartial.at(static_cast<int>(partial)).cPartial;

    // REMOVE WORKS
    for (int i = 0; i < mWork.size(); ++i) {
        if (index == mWork.at(i).cPartial) {
            mWork.removeAt(i);
            i--;
        }
    }

    mQuerys << QString("DELETE FROM works WHERE cod_partial = %1;").arg(index);

    // REMOVE TESTS
    for (int i = 0; i < mTest.size(); ++i) {
        if (index == mTest.at(i).cPartial) {
            mTest.removeAt(i);
            i--;
        }
    }

    mQuerys << QString("DELETE FROM tests WHERE cod_partial = %1;").arg(index);

    // REMOVE PARTIAL
    mPartial.removeAt(static_cast<int>(partial));

    mQuerys << QString("DELETE FROM partials WHERE cod_partial = %1;").arg(index);
}

void KlassProyect::removeWork(uint work)
{
    if (mWork.isEmpty() || mWork.size() < static_cast<int>(work)) {
        qDebug() << QObject::tr("Error: no existe un acumulativo a eliminar.") << endl;
        return;
    }

    int index = static_cast<int>(work);
    uint partialWork = mWork.at(index).cPartial;
    QString nameWork = mWork.at(index).wName;
    QString descWork = mWork.at(index).wDesc;

    for (int i = 0; i < mWork.size(); ++i) {
        if (mWork.at(i).cPartial == partialWork && mWork.at(i).wName == nameWork && mWork.at(i).wDesc == descWork) {
            mWork.removeAt(i);
            i--;
        }
    }

    mQuerys << QString("DELETE FROM works WHERE cod_partial = %1 AND name_work = '%2' AND desc_work = '%3';")
               .arg(partialWork).arg(nameWork).arg(descWork);
}

void KlassProyect::removeTest(uint test)
{
    if (mTest.isEmpty() || mTest.size() < static_cast<int>(test)) {
        qDebug() << QObject::tr("Error: no existe un examen a eliminar.") << endl;
        return;
    }

    int index = static_cast<int>(test);
    uint partialTest = mTest.at(index).cPartial;
    QString nameTest = mTest.at(index).tName;
    QString descTest = mTest.at(index).tDesc;

    for (int i = 0; i < mTest.size(); ++i) {
        if (mTest.at(i).cPartial == partialTest && mTest.at(i).tName == nameTest && mTest.at(i).tDesc == descTest) {
            mTest.removeAt(i);
            i--;
        }
    }

    mQuerys << QString("DELETE FROM tests WHERE cod_partial = %1 AND name_test = '%2' AND desc_test = '%3';")
               .arg(partialTest).arg(nameTest).arg(descTest);
}

QString KlassProyect::getName() const
{
    return mName;
}

QString KlassProyect::getCode() const
{
    return mCode;
}

QString KlassProyect::getSection() const
{
    return mSection;
}

QString KlassProyect::getFile() const
{
    return mFile;
}

int KlassProyect::sizeStudents() const
{
    return mStudent.size();
}

int KlassProyect::sizePartials() const
{
    return mPartial.size();
}

int KlassProyect::sizeWorks() const
{
    return mWork.size();
}

int KlassProyect::sizeTests() const
{
    return mTest.size();
}

void KlassProyect::saveProyect()
{
    QSqlDatabase Save = QSqlDatabase::addDatabase("QSQLITE");
    Save.setDatabaseName(mFile);

    if (!Save.open()) {
        qDebug() << QObject::tr("Error: no se pudo abrir el archivo") << endl << Save.lastError();
        return;
    }

    QSqlQuery q;

    if (!q.exec("CREATE TABLE IF NOT EXISTS datafile ("
                "name_file TEXT NOT NULL,"
                "code_file TEXT NOT NULL,"
                "sect_file TEXT NOT NULL,"
                "last_student INTEGER,"
                "last_partial INTEGER,"
                "last_work INTEGER,"
                "last_test INTEGER"
                ");")) {
        qDebug() << QObject::tr("Error: no se pudo crear la tabla «datafile»") << endl << q.lastError().text();
        return;
    }
    if (!q.exec("CREATE TABLE IF NOT EXISTS students ("
               "cod_student INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
               "name_student TEXT NOT NULL, "
               "id_student TEXT NOT NULL);")) {
        qDebug() << QObject::tr("Error: no se pudo crear la tabla «students».") << endl << q.lastError().text();
        return;
    }
    if (!q.exec("CREATE TABLE IF NOT EXISTS partials ("
               "cod_partial INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
               "name_partial TEXT NOT NULL, "
               "desc_partial TEXT, "
               "pond_partial REAL);")) {
        qDebug() << QObject::tr("Error: no se pudo crear la tabla «partials».") << endl << q.lastError().text();
        return;
    }
    if (!q.exec("CREATE TABLE IF NOT EXISTS works ("
               "cod_work INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
               "cod_student INTEGER NOT NULL, "
               "cod_partial INTEGER NOT NULL, "
               "name_work TEXT NOT NULL, "
               "desc_work TEXT, "
               "pond_work REAL, "
               "note_work REAL, "
               "FOREIGN KEY ('cod_student') REFERENCES 'students'('cod_student'), "
               "FOREIGN KEY ('cod_partial') REFERENCES 'partials'('cod_partial')"
               ");")) {
        qDebug() << QObject::tr("Error: no se pudo crear la tabla «works».") << endl << q.lastError().text();
        return;
    }
    if (!q.exec("CREATE TABLE IF NOT EXISTS tests ("
               "cod_test INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
               "cod_student INTEGER NOT NULL, "
               "cod_partial INTEGER NOT NULL, "
               "name_test TEXT NOT NULL, "
               "desc_test TEXT, "
               "pond_test REAL, "
               "note_test REAL, "
               "FOREIGN KEY ('cod_student') REFERENCES 'students'('cod_student'),"
               "FOREIGN KEY ('cod_partial') REFERENCES 'partials'('cod_partial')"
               ");")) {
        qDebug() << QObject::tr("Error: no se pudo crear la tabla «tests».") << endl << q.lastError().text();
        return;
    }

    // SAVE PROYECT
    for (int i = 0; i < mQuerys.size(); ++i) {
        if (!q.exec(mQuerys.at(i))) {
            qDebug() << QObject::tr("Error: no se pudo insertar a la tabla") << endl << q.lastError().text();
            return;
        }
    }

    if (!q.exec(QString("UPDATE datafile SET last_student = %1, last_partial = %2, last_work = %3, last_test = %4 "
                        "WHERE code_file = '%5';").arg(lastStudent).arg(lastPartial).arg(lastWork).arg(lastTest).arg(mCode))) {
        qDebug() << QObject::tr("Error: no se pudo insertar en la tabla «datafile»") << endl << q.lastError();
        return;
    }

    mQuerys.clear();
    Save.close();
}

void KlassProyect::loadProyect()
{
    QSqlDatabase Load = QSqlDatabase::addDatabase("QSQLITE");
    Load.setDatabaseName(mFile);

    if (!Load.open()) {
        qDebug() << QObject::tr("Error: no se pudo abrir el archivo.") << endl << Load.lastError().text();
        return;
    }

    clearMembers();

    QSqlQuery q;

    // LOAD NAME PROYECT
    if (!q.exec("SELECT name_file, code_file, sect_file, last_student, last_partial, last_work, last_test FROM datafile;")) {
        qDebug() << QObject::tr("Error: No se pudo obtener nada de la tabla «datafile»") << endl << q.lastError().text();
        return;
    }

    q.next();
    mName = q.value(0).toString();
    mCode = q.value(1).toString();
    mSection = q.value(2).toString();
    lastStudent = q.value(3).toInt();
    lastPartial = q.value(4).toInt();
    lastWork = q.value(5).toInt();
    lastTest = q.value(6).toInt();

    // LOAD STUDENTS
    if (!q.exec("SELECT cod_student, name_student, id_student FROM students;")) {
        qDebug() << QObject::tr("Error: No se pudo obtener nada de la tabla «students».") << endl << q.lastError().text();
        return;
    }

    while (q.next()) {
        Student addStudent;
        addStudent.cStudent = q.value(0).toUInt();
        addStudent.sName = q.value(1).toString();
        addStudent.sId = q.value(2).toString();
        mStudent.append(addStudent);
    }

    // LOAD PARTIALS
    if (!q.exec("SELECT cod_partial, name_partial, desc_partial, pond_partial FROM partials;")) {
        qDebug() << QObject::tr("Error: no se pudo obtener nada de la tabla «partials».") << endl << q.lastError().text();
        return;
    }

    while (q.next()) {
        Partial addPartial;
        addPartial.cPartial = q.value(0).toUInt();
        addPartial.pName = q.value(1).toString();
        addPartial.pDesc = q.value(2).toString();
        addPartial.pPond = q.value(3).toDouble();
        mPartial.append(addPartial);
    }

    // LOAD WORKS
    if (!q.exec("SELECT cod_work, cod_student, cod_partial, name_work, desc_work, pond_work, note_work FROM works;")) {
        qDebug() << QObject::tr("Error: no se pudo obtener nada de la tabla «works».") << endl << q.lastError().text();
        return;
    }

    while (q.next()) {
        Work addWork;
        addWork.cWork = q.value(0).toUInt();
        addWork.cStudent = q.value(1).toUInt();
        addWork.cPartial = q.value(2).toUInt();
        addWork.wName = q.value(3).toString();
        addWork.wDesc = q.value(4).toString();
        addWork.wPond = q.value(5).toDouble();
        addWork.wNote = q.value(6).toDouble();
        mWork.append(addWork);
    }

    // LOAD TESTS
    if (!q.exec("SELECT cod_test, cod_student, cod_partial, name_test, desc_test, pond_test, note_test FROM tests;")) {
        qDebug() << QObject::tr("Error: no se pudo obtener nada de la tabla «tests».") << endl << q.lastError().text();
        return;
    }

    while (q.next()) {
        Test addTest;
        addTest.cTest = q.value(0).toUInt();
        addTest.cStudent = q.value(1).toUInt();
        addTest.cPartial = q.value(2).toUInt();
        addTest.tName = q.value(3).toString();
        addTest.tDesc = q.value(4).toString();
        addTest.tPond = q.value(5).toDouble();
        addTest.tNote = q.value(6).toDouble();
        mTest.append(addTest);
    }

    Load.close();
}

void KlassProyect::clearMembers()
{
    mName.clear();
    mStudent.clear();
    mPartial.clear();
    mWork.clear();
    mTest.clear();
    mQuerys.clear();
}

Student KlassProyect::getStudent(uint student) const
{
    int index = static_cast<int>(student);

    return mStudent.at(index);
}

Partial KlassProyect::getPartial(uint partial) const
{
    int index = static_cast<int>(partial);

    return mPartial.at(index);
}

Work KlassProyect::getWork(uint work) const
{
    int index = static_cast<int>(work);

    return mWork.at(index);
}

Test KlassProyect::getTest(uint test) const
{
    int index = static_cast<int>(test);

    return mTest.at(index);
}
