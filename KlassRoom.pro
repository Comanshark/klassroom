#-------------------------------------------------
#
# Project created by QtCreator 2019-05-04T18:13:40
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = KlassRoom
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        aboutklassroom.cpp \
        comboboxdelegate.cpp \
        editpartial.cpp \
        editstudent.cpp \
        klassproyect.cpp \
        main.cpp \
        mainwindow.cpp \
        newdialog.cpp \
        newklass.cpp \
        panelpartial.cpp \
        panelstudent.cpp \
        paneltest.cpp \
        panelwork.cpp \
        spindelegate.cpp \
        startklass.cpp

HEADERS += \
        aboutklassroom.h \
        comboboxdelegate.h \
        editpartial.h \
        editstudent.h \
        klassdatatype.h \
        klassproyect.h \
        mainwindow.h \
        newdialog.h \
        newklass.h \
        panelpartial.h \
        panelstudent.h \
        paneltest.h \
        panelwork.h \
        spindelegate.h \
        startklass.h

FORMS += \
        aboutklassroom.ui \
        editpartial.ui \
        editstudent.ui \
        mainwindow.ui \
        newdialog.ui \
        newklass.ui \
        panelpartial.ui \
        panelstudent.ui \
        paneltest.ui \
        panelwork.ui \
        startklass.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    klassresource.qrc
