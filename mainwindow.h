#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "klassproyect.h"

class StartKlass;
class PanelStudent;
class PanelPartial;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_actionNew_triggered();

    void on_actionOpen_triggered();

    void on_actionSave_triggered();

    void on_actionSaveAs_triggered();

    void on_actionStudent_triggered();

    void on_actionPartial_triggered();

    void on_actionWork_triggered();

    void on_actionTest_triggered();

    void on_actionHelp_triggered();

    void on_actionQuit_triggered();

    void on_actionAbout_triggered();

    void on_actionFrist_triggered();

private:
    // METHODS MEMBERS    
    void setChecked(int checked);

    // PROPERTY MEMBERS
    Ui::MainWindow *ui;
    KlassProyect *Proyect;
    enum Panel {STUDENT, PARTIAL, WORK, TEST, HELP, START};

    // UI FORMS
    StartKlass *mStart;
};

#endif // MAINWINDOW_H
