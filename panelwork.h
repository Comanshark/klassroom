#ifndef PANELWORK_H
#define PANELWORK_H

#include <QWidget>

class KlassProyect;
class QStandardItemModel;
class QStandardItem;

namespace Ui {
class PanelWork;
}

class PanelWork : public QWidget
{
    Q_OBJECT

public:
    explicit PanelWork(KlassProyect * const proyect, QWidget *parent = nullptr);
    ~PanelWork();

private slots:
    void on_txtSearch_textChanged(const QString &arg1);
    void updateData(QStandardItem *item);
    void searchModelChanged(QStandardItem *item);

private:
    // METHODS MEMBERS
    void loadTableView();

    // PROPERTY MEMBERS
    Ui::PanelWork *ui;
    KlassProyect * const mProyect;
    QStandardItemModel *mModel;
    QStringList HorizontalLabels;
};

#endif // PANELWORK_H
