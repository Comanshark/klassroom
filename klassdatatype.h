#ifndef KLASSDATATYPE_H
#define KLASSDATATYPE_H

#include <QString>

struct Student {    
    uint cStudent;
    QString sName;
    QString sId;
};

struct Partial {
    uint cPartial;
    QString pName;
    QString pDesc;
    double pPond;
};

struct Work {
    uint cWork;
    uint cPartial;
    uint cStudent;
    QString wName;
    QString wDesc;
    double wPond;
    double wNote;
};

struct Test {    
    uint cTest;
    uint cPartial;
    uint cStudent;
    QString tName;
    QString tDesc;
    double tPond;
    double tNote;
};

#endif // KLASSDATATYPE_H
