#ifndef PANELTEST_H
#define PANELTEST_H

#include <QWidget>

class KlassProyect;
class QStandardItemModel;
class QStandardItem;

namespace Ui {
class PanelTest;
}

class PanelTest : public QWidget
{
    Q_OBJECT

public:
    explicit PanelTest(KlassProyect * const proyect, QWidget *parent = nullptr);
    ~PanelTest();

private slots:
    void on_txtSearch_textChanged(const QString &arg1);
    void noteUpdate(QStandardItem *item);
    void searchModelChanged(QStandardItem *item);

private:
    // METHODS MEMBERS
    void loadTableView();

    // PROPERTY MEMBERS
    Ui::PanelTest *ui;
    KlassProyect * const mProyect;
    QStandardItemModel *mModel;
    QStringList HorizontalLabels;
};

#endif // PANELTEST_H
