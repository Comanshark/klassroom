#include "editpartial.h"
#include "ui_editpartial.h"

#include <QInputDialog>
#include <QMessageBox>

EditPartial::EditPartial(QStringList partials, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditPartial)
{
    ui->setupUi(this);

    ui->cbPartial->addItems(partials);
    ui->cbPartial->setCurrentIndex(0);
    ui->cbType->addItems({tr("Parcial"), tr("Acumulativo"), tr("Examen")});

    if (partials.isEmpty()) {
        ui->cbType->removeItem(2);
        ui->cbType->removeItem(1);
    }

    ui->cbType->setCurrentIndex(0);
}

EditPartial::EditPartial(QString name, QString desc, double pond, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditPartial)
{
    ui->setupUi(this);

    ui->cbType->setVisible(false);
    ui->lbType->setVisible(false);
    ui->cbPartial->setVisible(false);
    ui->lbPartial->setVisible(false);

    ui->txtName->setText(name);
    ui->txtDesc->setText(desc);
    ui->spinPond->setValue(pond);
}

EditPartial::~EditPartial()
{
    delete ui;
}

void EditPartial::setTitlte(QString type)
{
    ui->lbTitlePartial->setText(tr("Editar ") + type);
}

QString EditPartial::getName()
{
    return ui->txtName->text();
}

QString EditPartial::getDesc()
{
    return ui->txtDesc->toPlainText();
}

double EditPartial::getPond()
{
    return ui->spinPond->value();
}

int EditPartial::getPartial()
{
    return ui->cbPartial->currentIndex();
}

int EditPartial::getType()
{
    return ui->cbType->currentIndex();
}

void EditPartial::on_buttonBox_accepted()
{
    accept();
}

void EditPartial::on_buttonBox_rejected()
{
    reject();
}

void EditPartial::on_cbType_currentIndexChanged(int index)
{
    switch (index) {
    case 0:
        ui->cbPartial->setVisible(false);
        ui->lbPartial->setVisible(false);
        break;
    default:
        ui->cbPartial->setVisible(true);
        ui->lbPartial->setVisible(true);
    }
}
