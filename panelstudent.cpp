#include "panelstudent.h"
#include "ui_panelstudent.h"

#include "klassproyect.h"
#include "klassdatatype.h"
#include "editstudent.h"

#include <QStandardItemModel>
#include <QAbstractItemModel>
#include <QMessageBox>
#include <QDialogButtonBox>

PanelStudent::PanelStudent(KlassProyect * const proyect, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PanelStudent),
    mProyect(proyect),
    mModel(new QStandardItemModel(this))
{    
    ui->setupUi(this);

    mModel->setHorizontalHeaderLabels({tr("No. de cuenta"), tr("Nombre"), tr("Nota final"), tr("Observación")});
    mModel->setColumnCount(4);
    refreshTable();
}

PanelStudent::~PanelStudent()
{
    delete ui;    
}

void PanelStudent::refreshTable()
{
    // CALCULATE THE FINAL NOTE
    QVector<double> Note;

    for (int i = 0; i < mProyect->sizeStudents(); ++i) {
        uint code = mProyect->getStudent(static_cast<uint>(i)).cStudent;
        Note.append(0.0);

        for (int j = 0; j < mProyect->sizeWorks(); ++j) {
            Work getWork = mProyect->getWork(static_cast<uint>(j));
            double pondPartial = 0;

            if (getWork.cStudent == code){
                for (int codPartial = 0; codPartial < mProyect->sizePartials(); ++codPartial) {
                    if (getWork.cPartial == mProyect->getPartial(static_cast<uint>(codPartial)).cPartial)
                        pondPartial = mProyect->getPartial(static_cast<uint>(codPartial)).pPond;
                }

                Note[i] += (pondPartial != 0.0 ? getWork.wNote / 100 * (getWork.wPond / 100 * pondPartial) : 0);
            }
        }

        for (int j = 0; j < mProyect->sizeTests(); ++j) {
            Test getTest = mProyect->getTest(static_cast<uint>(j));
            double pondPartial = 0;

            if (getTest.cStudent == code) {
                for (int codPartial = 0; codPartial < mProyect->sizePartials(); ++codPartial) {
                    if (getTest.cPartial == mProyect->getPartial(static_cast<uint>(codPartial)).cPartial)
                        pondPartial = mProyect->getPartial(static_cast<uint>(codPartial)).pPond;
                }

                Note[i] += (pondPartial != 0.0 ? getTest.tNote / 100 * (getTest.tPond / 100 * pondPartial ) : 0);
            }
        }
    }

    // INSERT IN THE TABLEVIEW

    for (int i = 0; i < mProyect->sizeStudents(); ++i) {
        QStandardItem *id = new QStandardItem(mProyect->getStudent(static_cast<uint>(i)).sId);
        QStandardItem *name = new QStandardItem(mProyect->getStudent(static_cast<uint>(i)).sName);
        QStandardItem *note = new QStandardItem(QString::number(Note.at(i)));
        QStandardItem *obs = new QStandardItem;

        mModel->appendRow({id, name, note, obs});
    }

    ui->tvStudent->setModel(mModel);
    ui->tvStudent->resizeColumnsToContents();
}

void PanelStudent::on_btnAddStudent_clicked()
{
    EditStudent editStudent(this);

    editStudent.setWindowTitle(tr("Añadir alumno"));

    if (editStudent.exec() == QDialog::Rejected) {
        return;
    }

    mProyect->setStudent(editStudent.getName(), editStudent.getId());
    mModel->appendRow({new QStandardItem(editStudent.getId()), new QStandardItem(editStudent.getName()), new QStandardItem("0")});
}

void PanelStudent::on_btnRemoveStudent_clicked()
{
    const int RowSelected = ui->tvStudent->currentIndex().row();

    // NOT SELECTION
    if (RowSelected == -1) {
        QMessageBox::information(this, tr("Ninguna selección"), tr("No ha seleccionado ningún alumno."));
        return;
    }

    // REMOVE ROWS
    if (ui->tvStudent->model() == mModel) { // If not searching.
        mModel->removeRow(RowSelected);
        mProyect->removeStudent(static_cast<uint>(RowSelected));
    }
    else {
        QString searchId = ui->tvStudent->model()->index(RowSelected, 0).data().toString();
        QString searchName = ui->tvStudent->model()->index(RowSelected, 1).data().toString();
        QString searchNote = ui->tvStudent->model()->index(RowSelected, 2).data().toString();

        for (int i = 0; i < mModel->rowCount(); ++i) { // If searching.
            QString Id = mModel->item(i, 0)->text();
            QString Name = mModel->item(i, 1)->text();
            QString Note = mModel->item(i, 2)->text();

            if (searchId == Id && searchName == Name && searchNote == Note) {                
                mModel->removeRow(i);
                ui->tvStudent->model()->removeRow(RowSelected);
                mProyect->removeStudent(static_cast<uint>(i));
                break;
            }
        }

    }

}

void PanelStudent::on_txtSearch_textChanged(const QString &changedText)
{
    QStandardItemModel *searchModel = new QStandardItemModel(this);
    const int ROWS = mModel->rowCount();

    // IF NOT SEARCHING
    if (changedText.isEmpty()) {
        ui->tvStudent->setModel(mModel);
        ui->tvStudent->update();
        return;
    }

    // COPY THE SAME STRUCTURE
    searchModel->setHorizontalHeaderLabels({tr("No. de cuenta"), tr("Nombre"), tr("Nota final"), tr("Observación")});
    searchModel->setColumnCount(4);

    // CREATE A COPY MODEL FOR THE SEARCH TEXT
    for (int i = 0; i < ROWS; ++i) {
        QString oId = mModel->item(i, 0)->text();
        QString oName = mModel->item(i, 1)->text();
        QString oNote = mModel->item(i, 2)->text();
        QString oObs = mModel->item(i, 3)->text();

        if (oId.contains(changedText, Qt::CaseInsensitive) || oName.contains(changedText, Qt::CaseInsensitive)
                || oNote.contains(changedText, Qt::CaseInsensitive)) {
            searchModel->appendRow({new QStandardItem(oId), new QStandardItem(oName), new QStandardItem(oNote), new QStandardItem(oObs)});
        }

    }

    // UPDATE THE TABLEVIEW
    ui->tvStudent->setModel(searchModel);
    ui->tvStudent->update();
}

void PanelStudent::on_tvStudent_doubleClicked(const QModelIndex &index)
{
    const int RowSelected = index.row();

    // NOT SELECTION
    if (RowSelected == -1) {
        QMessageBox::information(this, tr("Ninguna selección"), tr("No ha seleccionado ningún alumno."));
        return;
    }

    // EDIT ROW
    if  (ui->tvStudent->model() == mModel) {
        QString editId = mModel->item(RowSelected, 0)->text();
        QString editName = mModel->item(RowSelected, 1)->text();
        QString Obs;
        EditStudent editor(editId, editName, this);
        editor.setWindowTitle(tr("Editar ") + editName);
        editor.setObs(mModel->item(RowSelected, 2)->text().toDouble());

        if (editor.exec() == QDialog::Rejected) {
            return;
        }

        editId = editor.getId();
        editName = editor.getName();
        Obs = editor.getObs();

        mModel->item(RowSelected, 0)->setText(editId);
        mModel->item(RowSelected, 1)->setText(editName);
        mModel->item(RowSelected, 3)->setText(Obs);

        mProyect->updateStudent(static_cast<uint>(RowSelected), editName, editId);
    }
    else {
        QString searchId = ui->tvStudent->model()->index(RowSelected, 0).data().toString();
        QString searchName = ui->tvStudent->model()->index(RowSelected, 1).data().toString();
        QString searchNote = ui->tvStudent->model()->index(RowSelected, 2).data().toString();

        for (int i = 0; i < mModel->rowCount(); ++i) { // If searching.
            QString Id = mModel->item(i, 0)->text();
            QString Name = mModel->item(i, 1)->text();
            QString Note = mModel->item(i, 2)->text();
            QString Obs;

            if (searchId == Id && searchName == Name && searchNote == Note) {
                EditStudent editor(Id, Name, this);
                editor.setObs(Note.toDouble());

                if (editor.exec() == QDialog::Rejected) {
                    return;
                }

                Id = editor.getId();
                Name = editor.getName();
                Obs = editor.getObs();

                mModel->item(i, 0)->setText(Id);
                ui->tvStudent->model()->setData(ui->tvStudent->model()->index(i, 0), Id);
                mModel->item(i, 1)->setText(Name);
                ui->tvStudent->model()->setData(ui->tvStudent->model()->index(i, 1), Name);
                mModel->item(i, 3)->setText(Obs);
                ui->tvStudent->model()->setData(ui->tvStudent->model()->index(i, 3), Obs);

                mProyect->updateStudent(static_cast<uint>(i), Id, Name);
                break;
            }
        }
    }

}
