#ifndef EDITSTUDENT_H
#define EDITSTUDENT_H

#include <QDialog>

namespace Ui {
class EditStudent;
}

class EditStudent : public QDialog
{
    Q_OBJECT

public:
    explicit EditStudent(QWidget *parent = nullptr);
    explicit EditStudent(QString id, QString name, QWidget *parent = nullptr);
    ~EditStudent();

    void setObs(double note);
    QString getName();
    QString getId();
    QString getObs();

private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::EditStudent *ui;
};

#endif // EDITSTUDENT_H
